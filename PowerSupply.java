package ua.org.oa.makarenkoas;

/*
 * @version 1.0 22 Feb 2017 
 * @author Makarenko Alexander
 */

public class PowerSupply {
	public static String name;

	public String formFactor;

	public static int inputVoltage;

	public int powerSupplyUnitPower;		// The calculation of required power

	public int centralProcessorUnitPower;	// CPU power consumption

	public int videoCardPower;				// VGA power consumption

	public int motherboardPower;			// Motherboard power consumption

	public int randomAccessMemoryPower;		// RAM power consumption

	public PowerSupply(String newName, String newFormFactor,
					   int newInputVoltage, int newPowerSupplyUnitPower) { 
		
		// PowerSupply constructor
		
		name = newName;
		formFactor = newFormFactor;
		inputVoltage = newInputVoltage;
		powerSupplyUnitPower = newPowerSupplyUnitPower;

	}

	public static int calculatePowerSupplyUnitPower(int centralProcessorUnitPower, int videoCardPower,
			int motherboardPower, int randomAccessMemoryPower) {
		
		// Method to calculate required power
		
		int powerSupplyUnitPower = centralProcessorUnitPower + videoCardPower + motherboardPower
				+ randomAccessMemoryPower;

		System.out.println("PSU required or higher than: " + powerSupplyUnitPower + " Watts");

		return powerSupplyUnitPower;
	}

	public String toString() {
		
		// Overloaded method toString
		
		return "PSU Info: (" + "name: " + name + "/ form factor " + formFactor
				+ "/ input voltage " + inputVoltage	+ " V/ power " + powerSupplyUnitPower + " Watts )";
	}

}
