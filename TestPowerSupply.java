package ua.org.oa.makarenkoas;

/*
 * @version 1.0 22 Feb 2017 
 * @author Makarenko Alexander
 */

public class TestPowerSupply {

	public static void main(String[] args) {

		PowerSupply coolerMaster = new PowerSupply("Cooler Master", "ATX", 220,
				PowerSupply.calculatePowerSupplyUnitPower(150, 75, 250, 20));
		System.out.println(coolerMaster);
				
		PowerSupply chieftec = new PowerSupply("Chieftec", "ATX", 220,
				PowerSupply.calculatePowerSupplyUnitPower(100, 50, 150, 10));
		System.out.println(chieftec);
	}

}
